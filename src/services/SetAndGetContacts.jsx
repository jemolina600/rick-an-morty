import React from 'react';
import {action, makeObservable, observable} from "mobx";

 class SetAndGetContacts{

     KEY = 'contactsApp.contacts';
     constructor(){
         if(this.contacts = []){
             this.contactGetStore()
                if(this.contacts = []){
                    this.contacts = [
                        {id:0, name:'prueba'},
                        {id:1, name:'prueba2'}
                    ];
                }
         }

         makeObservable(this, {
             contacts: observable,
             handleContactAdd: action.bound,
             handleContactRemoveAll: action.bound,
             contactSetStore: action.bound,
             contactGetStore: action.bound
         })
     }

    contactGetStore  () {
        const storedTask = JSON.parse(localStorage.getItem(this.KEY));
        if(storedTask){
            this.contacts = storedTask;
        }
    }


    contactSetStore (){
        localStorage.setItem(this.KEY, JSON.stringify(this.contacts))
    }
    handleContactAdd(data, telRef, dirRef){
        const telefono = telRef.current.value;
        const direccion = dirRef.current.value;
        if (telefono === '' && direccion === '')return;
        this.contacts.push({id:this.contacts.length+1, name: data.name, phone: telefono, direction: direccion, image: data.image, status: data.status, gender: data.gender, species: data.species, origin:data.origin} )
        this.contactSetStore();
    }
     handleContactRemoveAll(id){
         const newContacts = this.contacts.filter((contact) => contact.id !== id )
         this.contacts = newContacts;
     }
}
const data = new SetAndGetContacts();
export const CounterStoreContext = React.createContext(data);
export const useCounterStore = () => React.useContext(CounterStoreContext)
