
import React from 'react';
import { observable, action, makeObservable } from 'mobx';


class GetData {
    activeCards = false;

    constructor() {
        makeObservable(this, {
            activeCards: observable,
            switchCards: action.bound
        })
    }

    switchCards(data) {
        this.activeCards = data;
    }
}
const observableStore = new GetData();


export const CounterStoreContext = React.createContext(observableStore);
export const useCounterStore = () => React.useContext(CounterStoreContext)
