import React, {useState, useRef} from 'react';
import { Card, Button, Modal } from 'react-bootstrap';
import { useCounterStore as setAndGetStore} from '../services/SetAndGetContacts';
import { observer } from 'mobx-react';

export const CardUser = observer(({user}) => {

    const { handleContactAdd } = setAndGetStore();

    const telRef = useRef();
    const dirRef = useRef();
        const {image, name, status, species, gender, origin} = user;
        const state = {
            alive: 'Alive',
            dead: 'Dead',
            unknown: 'Unknown'
        }
        const [show, setShow] = useState(false);

        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);

        const handleSubmit = () =>{
            handleContactAdd(user,telRef, dirRef);
            telRef.current.value = null;
            dirRef.current.value = null;
            handleClose();
        }
        return (
            <div>
                <Card style={{ width: '15rem', backgroundColor: '#000000'}}>
                    <Card.Img variant="top" src={image} />
                    <Card.Body>
                        <Card.Title style={{ color: '#fff', fontSize: '1.5em'}}>{name}</Card.Title>
                        <span>
                        { state.alive  === status ?
                            <Card.Text style={{ color: '#9bff7c', fontSize: '1.3em'}}>{status}</Card.Text>
                            : state.dead === status ?
                                <Card.Text style={{ color: '#ff7070', fontSize: '1.3em'}}>{status}</Card.Text>
                                :
                                <Card.Text style={{ color: '#fff', fontSize: '1.3em'}}>{status}</Card.Text>
                        }
                    </span>
                        <Card.Text style={{ color: '#fff', fontSize: '1.3em'}}>{species}</Card.Text>
                        <Card.Text style={{ color: '#fff', fontSize: '1.3em'}}>{gender}</Card.Text>
                        <Card.Text style={{ color: '#fff', fontSize: '1.3em'}}>{origin.name}</Card.Text>
                        <Button variant="primary" onClick={handleShow}>Agregar Contacto</Button>
                    </Card.Body>
                </Card>
                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Agregar Contacto</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form >

                            <input disabled className='form-control' type="text" value={name} placeholder={name}/>
                            <hr/>
                            <input ref={telRef} className='form-control' type="text" placeholder='Agrega Telefono'/>
                            <hr/>
                            <input ref={dirRef} className='form-control' type="text" placeholder='Agrega Direccion'/>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={handleSubmit}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
)
