import React, {useEffect} from 'react';
import {CardUser} from './CardUser';
import './CardsStyles.css'
import { observer } from 'mobx-react';
import { useCounterStore} from '../services/Observable';

export const CardsUsers = observer(({users}) =>{
    const { activeCards } = useCounterStore();
        useEffect(() =>{
            console.log(activeCards)
            if (activeCards) {
                document.getElementById('cards').classList.add('isActiveMenu');
                document.getElementById('cards').classList.remove('inActiveMenu');
            } else if (!activeCards){
                document.getElementById('cards').classList.add('inActiveMenu');
                document.getElementById('cards').classList.remove('isActiveMenu');
            }
        })

        return (
            <div id='cards' className='cards'>
                {users.map((user) =>
                    <div style={{margin: '1em', display:'inline-block'}} key={user.id}>
                        <CardUser key={user.id} user={user}/>
                    </div>
                )}
            </div>


        )
    }
)
