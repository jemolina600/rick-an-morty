import React, {useState, useRef} from 'react';
import {Contact} from './Contact';
import {faUserPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Button, Modal} from "react-bootstrap";
import { useCounterStore as setAndGetStore} from '../services/SetAndGetContacts';
import { observer } from 'mobx-react';

export const Contacts = observer(() => {
    const telRef = useRef();
    const dirRef = useRef();
    const nameRef = useRef();
    const { contacts, handleContactAdd } = setAndGetStore();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    let dataJson = {name: null, image: null, status: null, gender: null, species: null, origin:null};
    const handleSubmit = () =>{
        dataJson.name = nameRef.current.value;
        handleContactAdd(dataJson, telRef, dirRef);
        telRef.current.value = null;
        dirRef.current.value = null;
        handleClose();
    }
    const handleUpdate = () =>{
        dataJson.name = nameRef.current.value;
        handleContactAdd(dataJson, telRef, dirRef);
        telRef.current.value = null;
        dirRef.current.value = null;
        handleClose();
    }
    return (
        <div>{console.log(contacts)}
            <Button variant="primary" onClick={handleShow}><FontAwesomeIcon icon={faUserPlus} style={{fontSize: '2em'}}/></Button>
            <div style={{overflowY: 'scroll', height: '32em',marginTop:'1em'}}>
                {contacts.map((contact) =>
                    <div  >
                        <Contact key={contact.id} contact={contact}/>
                    </div>

                )}
            </div>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Agregar Contacto</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form >

                        <input ref={nameRef} className='form-control' type="text"  placeholder='nombre'/>
                        <hr/>
                        <input ref={telRef} className='form-control' type="text" placeholder='Agrega Telefono'/>
                        <hr/>
                        <input ref={dirRef} className='form-control' type="text" placeholder='Agrega Direccion'/>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>

            x

        </div>
    )
})
