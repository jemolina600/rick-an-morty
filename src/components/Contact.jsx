import React, {useRef, useState} from 'react';
import'./CardsStyles.css';
import {Button, Modal} from "react-bootstrap";
import { useCounterStore as setAndGetStore} from '../services/SetAndGetContacts';
import {observer} from "mobx-react";


export const Contact = observer(({contact}) => {
    const {  handleContactAdd, handleContactRemoveAll } = setAndGetStore();
        const {id, name, phone, direction, image} = contact;
        const telRef = useRef();
        const dirRef = useRef();
        const nameRef = useRef();
        const [show, setShow] = useState(false);
        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);
        const handleSubmit = () =>{
            handleContactAdd(contact, telRef, dirRef);
            handleContactRemoveAll(id);
            handleClose();
        }
        const remove = () => {
            handleContactRemoveAll(id);
        }
         return (
            <div className='cards-horizontal' onClick={handleShow}>
                <div className='cards-image'>
                    {image ? <img src={image} alt="img-contacto" style={{width: '100%', height:'100%', borderRadius: '1em 0 0 1em'}}/>:null}
                </div>
                <div className='cards-content'>
                    <p className='cards-text'>Nombre: {name} </p>
                    <p className='cards-text'>Telefono: {phone}</p>
                    <p className='cards-text'>Direccion: {direction}</p>
                </div>
                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modificar Usuario</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form >
                            <input ref={nameRef} className='form-control' type="text" value={name}  placeholder='nombre'/>
                            <hr/>
                            <input ref={telRef} className='form-control' type="text" value={phone} placeholder='Agrega Telefono'/>
                            <hr/>
                            <input ref={dirRef} className='form-control' type="text" value={direction} placeholder='Agrega Direccion'/>
                        </form>
                        <hr/>
                        <Button variant="danger" onClick={remove}>
                            Delete
                        </Button>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={handleSubmit}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
)
