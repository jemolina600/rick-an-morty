import React, {useState, useEffect} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'normalize.css';
import {CardsUsers} from './components/CardsUsers';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAddressBook } from "@fortawesome/free-solid-svg-icons";
import { useCounterStore as observableStore} from './services/Observable';
import {Contacts} from './components/Contacts'

import axios from "axios";


export function App(){
    const { switchCards } = observableStore();
    const [users, setUsers] = useState([])
    const [show, setShow] = useState(false)

    useEffect(() => {
       userGet(1)
    }, []);

    const userGet = (page) => {
        axios.get(`https://rickandmortyapi.com/api/character`, { params: { page: page } }).then(res => {
            setUsers(res.data.results);
            console.log(res.data);

        });
    }

    let  active= true;
    const toggleActive = () =>{
        console.log('se ejecuto');
        if (active) {
            document.getElementById('menuContacts').classList.add('isActive');
            document.getElementById('menuContacts').classList.remove('inActive');
            active=!active;
            switchCards(active)
            setShow(!active);
        } else if (!active){
            document.getElementById('menuContacts').classList.add('inActive');
            document.getElementById('menuContacts').classList.remove('isActive');
            active = !active;
            switchCards(active)
            setShow(!active);
        }
    }


   return (
       <div style={{flexDirection:'row', display: 'flex'}}>
           <div id='menuContacts' className='menuContacts'>
               <Button variant="primary" onClick={ toggleActive }><FontAwesomeIcon icon={faAddressBook} style={{fontSize: '2em'}}/></Button>
                   {show ?<div>
                       <hr/> <Contacts></Contacts>
                   </div>  : null }
           </div>
           <div id="cardsUsers" style={{height: '95vh'}}>
               <CardsUsers users={users}/>
           </div>
       </div>
   );
}

